@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crear tasks</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif

                        <!-- New Task Form -->
                            <form action="/tasks" method="POST" class="form-horizontal">
                            {{ csrf_field() }}

                            <!-- Task Name -->
                                <div class="form-group">
                                    <label for="task-name" class="col-sm-3 control-label">Task</label>

                                    <div class="col-sm-6">
                                        <input type="text" name="name" id="task-name" class="form-control">
                                    </div>
                                </div>

                                <!-- Add Task Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fa fa-plus"></i> Add Task
                                        </button>
                                    </div>
                                </div>
                            </form>

                            @if (count($tasks) > 0)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Current Tasks
                                    </div>

                                    <div class="panel-body">
                                        <table class="table table-striped task-table">

                                            <!-- Table Headings -->
                                            <thead>
                                            <th>Task</th>
                                            <th>&nbsp;</th>
                                            </thead>

                                            <!-- Table Body -->
                                            <tbody>
                                            @foreach ($tasks as $task)
                                                <tr>
                                                    <!-- Task Name -->
                                                    <td class="table-text">
                                                        <div>{{ $task->name }}</div>
                                                    </td>
                                                    <!-- Edit Button -->
                                                    <td><a href="{{ route('tasks.edit',$task->id)}}" class="btn btn-primary">Edit</a></td>
                                                    {{--<td><a href="{{ url('/tasks/'.$task->id.'/edit')}}" class="btn btn-primary">Edit</a></td>--}}
                                                    <!-- Delete Button -->
                                                    <td>
                                                        <form action="/tasks/{{ $task->id }}" method="POST">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}

                                                            <button class="btn btn-primary">Delete Task</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="panel-body">
        <!-- Display Validation Errors -->


    </div>

    <!-- TODO: Current Tasks -->
@endsection