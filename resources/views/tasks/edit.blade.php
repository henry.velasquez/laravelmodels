@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Editar tasks</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif

                        <!-- New Task Form -->
                            <form action="/tasks/{{ $task->id }}" method="POST" class="form-horizontal">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <!-- Task Name -->
                                <div class="form-group">
                                    <label for="task-name" class="col-sm-3 control-label">Task</label>

                                    <div class="col-sm-6">
                                        <input type="text" name="name" id="task-name" value="{{ $task->name }}" class="form-control">
                                    </div>
                                </div>

                                <!-- Add Task Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fa fa-plus"></i> Edit Task
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="panel-body">
        <!-- Display Validation Errors -->


    </div>

    <!-- TODO: Current Tasks -->
@endsection